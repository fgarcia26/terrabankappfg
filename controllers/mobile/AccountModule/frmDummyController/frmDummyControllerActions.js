define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnShowHome **/
    AS_Button_b4fb0c86c9a44077a94c01c66d8170d2: function AS_Button_b4fb0c86c9a44077a94c01c66d8170d2(eventobject) {
        var self = this;
        return self.getAccounts.call(this);
    },
    /** preShow defined for frmDummy **/
    AS_Form_i23f01dd64714d44855447c633e76baf: function AS_Form_i23f01dd64714d44855447c633e76baf(eventobject) {
        var self = this;
        kony.lang.setUncaughtExceptionHandler(GlobalExceptionHandler.exceptionHandler);
        try {
            var ApplicationManager = require('ApplicationManager');
            applicationManager = ApplicationManager.getApplicationManager();
        } catch (err) {
            throw GlobalExceptionHandler.addMessageAndActionForException(err, "kony.error.App_Initialisation_Failed", GlobalExceptionHandler.ActionConstants.BLOCK, arguments.callee.name);
        }
    }
});