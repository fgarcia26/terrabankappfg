define({ 

  loadAccountList: function() {
    var navManager = applicationManager.getNavigationManager();
    var resultData = navManager.getCustomInfo("frmChangeName");
    this.view.listSelect.masterData = resultData.masterData;
    this.view.forceLayout();
  },

  updateAccountName: function(){
    var accountId = this.view.lblAccountidVal.text;
    var accountName = this.view.tbxName.text;
    var accountModule = applicationManager.getModule("AccountModule");
    accountModule.presentationController.updateAccountName(accountId, accountName);
  }


});