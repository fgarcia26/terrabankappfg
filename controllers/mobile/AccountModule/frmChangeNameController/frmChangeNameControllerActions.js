define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onSelection defined for listSelect **/
    AS_ListBox_e8e51c6410554d2e93a1c477369eae26: function AS_ListBox_e8e51c6410554d2e93a1c477369eae26(eventobject) {
        var self = this;
        this.view.tbxName.text = this.view.listSelect.selectedKeyValue[1];
        this.view.tbxAccountId.text = this.view.listSelect.selectedKeyValue[0];
    },
    /** onClick defined for btnSubmit **/
    AS_Button_d9abad09f5ad4bcc9e01e38c441aaf15: function AS_Button_d9abad09f5ad4bcc9e01e38c441aaf15(eventobject) {
        var self = this;
        return self.updateAccountName.call(this);
    },
    /** preShow defined for frmChangeName **/
    AS_Form_h4c3df4bc0f746608a60047165c9bded: function AS_Form_h4c3df4bc0f746608a60047165c9bded(eventobject) {
        var self = this;
        return self.loadAccountList.call(this);
    }
});