define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for lblSend **/
    AS_Label_ga538b08f216445f8e17683a324a048b: function AS_Label_ga538b08f216445f8e17683a324a048b(eventobject, x, y) {
        var self = this;
        return self.displayTransferForm.call(this);
    },
    /** onTouchEnd defined for lblUpdateAccount **/
    AS_Label_e55e780c7e0f498486fdb4c85bd62809: function AS_Label_e55e780c7e0f498486fdb4c85bd62809(eventobject, x, y) {
        var self = this;
        return self.loadUpdateAccountForm.call(this);
    },
    /** onRowClick defined for segAccounts **/
    AS_Segment_cc162fb0cf664601b62b836ebbf9e81f: function AS_Segment_cc162fb0cf664601b62b836ebbf9e81f(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.getTransactions.call(this);
    },
    /** onClick defined for btnApplyLoan **/
    AS_Button_dc94aa85278c4340ae076c69b092f381: function AS_Button_dc94aa85278c4340ae076c69b092f381(eventobject) {
        var self = this;
        return self.displayLoanForm.call(this);
    },
    /** preShow defined for frmHome **/
    AS_Form_h5b65daf4b2e4e7b8bc1cc31213dd612: function AS_Form_h5b65daf4b2e4e7b8bc1cc31213dd612(eventobject) {
        var self = this;
        return self.showAccountData.call(this);
    }
});