define({ 

  showAccountData: function(){
    var navManager = applicationManager.getNavigationManager();
    var resultData = navManager.getCustomInfo("frmHome");
    var widgetDataMap = {
      "lblAccountid": "accountidmasked",
      "lblAccountname": "accountname",
      "lblBalance": "balance"
    };
    this.view.segAccounts.widgetDataMap = widgetDataMap;
    this.view.segAccounts.setData(resultData);
  },

  getTransactions: function() {
    var rowItem = this.view.segAccounts.selectedRowItems[0];
    alert("Selected Row Item . "+JSON.stringify(rowItem));
    var transactionModule = applicationManager.getModule("TransactionModule");
    transactionModule.presentationController.getTransactionsAndShow(rowItem);
  },

  displayTransferForm: function(){
    var transferModule = applicationManager.getModule("TransferModule");
    transferModule.presentationController.getAccountsandLoadTransferForm();
  },
  
  loadUpdateAccountForm: function() {
    var accModule = applicationManager.getModule("AccountModule");
    accModule.presentationController.navigateToUpdateAccForm();
  },

  displayLoanForm: function() {
    var loanModule = applicationManager.getModule("LoanModule");
    loanModule.presentationController.navigateToLoanForm();
  }
});	