define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTextChange defined for tbxTerms **/
    AS_TextField_fcba172b3dc54782a225a85aed6a8e29: function AS_TextField_fcba172b3dc54782a225a85aed6a8e29(eventobject, changedtext) {
        var self = this;
        return self.fillEmi.call(this);
    },
    /** onClick defined for btnSubmit **/
    AS_Button_ddf4e90728394d96af0926dcd472c023: function AS_Button_ddf4e90728394d96af0926dcd472c023(eventobject) {
        var self = this;
        return self.callApplyLoan.call(this);
    },
    /** preShow defined for frmLoan **/
    AS_Form_i14956cced2645e1835efaf783bfe6a8: function AS_Form_i14956cced2645e1835efaf783bfe6a8(eventobject) {
        var self = this;
        return self.displayLoanForm.call(this);
    }
});