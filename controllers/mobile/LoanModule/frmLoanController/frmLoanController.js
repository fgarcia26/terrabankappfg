define({ 

  displayLoanForm: function() {
    var navManager = applicationManager.getNavigationManager();
    var data = navManager.getCustomInfo("frmLoan");
    if (data.interestRate) {
      this.view.flxContent.isVisible = true;
      this.view.flxResult.isVisible = false;
      this.view.lblInterestValue.text = data.interestRate.interestRateValue;
    }
    if (data.loanReference){
      this.view.flxContent.isVisible = false;
      this.view.flxResult.isVisible = true;
      this.view.flxResult.rtxtResult.text = data.loanReference.message;
      this.view.forceLayout();
    }
  },
  
  fillEmi: function() {
    var amount = this.view.tbxAmount.text;
    var terms = this.view.tbxTerms.text;
    if (typeof amount === 'undefined' || amount === null) return;
    if (typeof terms === 'undefined' || terms === null) return;

    var emi = Math.ceil(amount/terms);
    this.view.lblEmiValue.text = emi;
  },
  
  callApplyLoan: function(){
    var amount = this.view.tbxAmount.text;
  	var terms = this.view.tbxTerms.text;
    var interest = this.view.lblInterestValue.text;
    var purpose = this.view.tbxPurpose.text;
  	var idnumber = this.view.tbxIdnumber.text;
    var phone = this.view.tbxPhone.text;
    var email = this.view.tbxEmail.text;
    var loanModule = applicationManager.getModule("LoanModule");
    loanModule.presentationController.applyLoan(amount, terms, interest, purpose, idnumber, phone, email);
  }
  
});