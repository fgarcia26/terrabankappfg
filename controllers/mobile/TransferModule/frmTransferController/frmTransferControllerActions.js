define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSubmit **/
    AS_Button_f9f2c4b2d87b4e869c514f019f354378: function AS_Button_f9f2c4b2d87b4e869c514f019f354378(eventobject) {
        var self = this;
        return self.getFormData.call(this);
    },
    /** onTouchEnd defined for lblBack **/
    AS_Label_f72721793d82436fbda4673f4e451756: function AS_Label_f72721793d82436fbda4673f4e451756(eventobject, x, y) {
        var self = this;
        return self.navigateToHomeUsingService.call(this);
    },
    /** preShow defined for frmTransfer **/
    AS_Form_f109f69547834a508bc400e6bb860e61: function AS_Form_f109f69547834a508bc400e6bb860e61(eventobject) {
        var self = this;
        return self.loadAccountList.call(this);
    }
});