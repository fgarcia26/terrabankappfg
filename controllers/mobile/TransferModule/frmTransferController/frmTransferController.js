define({ 

  loadAccountList: function() {
    var navManager = applicationManager.getNavigationManager();
    var resultData = navManager.getCustomInfo("frmTransfer");

    this.view.listFrom.masterData = resultData.masterData;
    this.view.listTo.masterData = resultData.masterData;
    this.view.forceLayout();
  },
  
  getFormData: function() {
    var fromaccountid = this.view.listFrom.selectedKey;
    var toaccountid = this.view.listTo.selectedKey;
    var amount = this.view.txtAmount.text;
    var currency = this.view.listCurrency.selectedKey;
    var transferModule = applicationManager.getModule("TransferModule");
    transferModule.presentationController.saveTransactionToDB(fromaccountid, toaccountid, amount, currency);
  },

  navigateToHomeUsingService: function() {
    var AccountModule =  applicationManager.getModule("AccountModule");
    AccountModule.presentationController.getAccountsandShowHome();
  }

 });