define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for lblBack **/
    AS_Label_dd4a864504f0416e8345ac1da08f29dc: function AS_Label_dd4a864504f0416e8345ac1da08f29dc(eventobject, x, y) {
        var self = this;
        return self.navigateToHomeUsingNavObj.call(this);
    },
    /** onTouchEnd defined for lblSend **/
    AS_Label_i37d41cc8b8147b9a720db7eb8812f0a: function AS_Label_i37d41cc8b8147b9a720db7eb8812f0a(eventobject, x, y) {
        var self = this;
        return self.displayTransferForm.call(this);
    },
    /** preShow defined for frmTransactions **/
    AS_Form_de69fbcc8daf4f4e82400f36179c5cbc: function AS_Form_de69fbcc8daf4f4e82400f36179c5cbc(eventobject) {
        var self = this;
        return self.showTransactionsData.call(this);
    }
});