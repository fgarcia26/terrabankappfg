define({ 
  showTransactionsData: function() {
    var navManager = applicationManager.getNavigationManager();
    var resultData = navManager.getCustomInfo("frmTransactions");

    var widgetDataMap = {
      "lblTid": "tid",
      "lblFrom": "fromaccountid",
      "lblTo": "toaccountid",
      "lblBalance": "amount",
      "lblNotes": "notes"
    };

    this.view.segTransactions.widgetDataMap = widgetDataMap;
    this.view.segTransactions.setData(resultData);
  },

  displayTransferForm: function(){
    var transferModule = applicationManager.getModule("TransferModule");
    transferModule.presentationController.getAccountsandLoadTransferForm();
  },

  navigateToHomeUsingNavObj: function() {
    var AccountModule = applicationManager.getModule("AccountModule");
    AccountModule.presentationController.navigateToHomeUsingNavObj();
  }
});