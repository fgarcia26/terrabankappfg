define({
  getTransactionsSC: function(response) {
    try {
      var length = response.length;
      for(var i = 0; i < length; i++) {
        var amount = response[i].amount;
        if((parseFloat(response[i].amount)) > 30) {
          response[i].amount = {
            "text": amount,
            "skin": "sknRedLabel"
          };
        }
      }
    } catch(ex) {
      kony.print("Error: " + ex.toString());
    }
    var navObj = applicationManager.getNavigationManager();
    navObj.setCustomInfo("frmTransactions", response);
    navObj.navigateTo("frmTransactions");
  },

  getTransactionsAndShow: function(rowItem) {
    alert("Transaction PC Extn");
    var transactionManager = applicationManager.getTransactionManager();
    transactionManager.getTransactions(rowItem, this.getTransactionsSC.bind(this), this.getTransactionsEC.bind(this));

  }
});