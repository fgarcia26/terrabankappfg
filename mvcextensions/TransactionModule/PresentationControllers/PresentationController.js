define([], function() {
  /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
  function Transaction_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(Transaction_PresentationController, kony.mvc.Presentation.BasePresenter);

  /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
  Transaction_PresentationController.prototype.initializePresentationController = function() {

  };

  Transaction_PresentationController.prototype.getTransactionsAndShow = function() {
    var transactionManager = applicationManager.getTransactionManager();
    transactionManager.getTransactions(this.getTransactionsSC.bind(this), this.getTransactionsEC.bind(this));
  };
  

  Transaction_PresentationController.prototype.getTransactionsSC = function(response) {
    var accNavObj = applicationManager.getNavigationManager();
    accNavObj.setCustomInfo("frmTransactions", response);
    accNavObj.navigateTo("frmTransactions");
  };

  Transaction_PresentationController.prototype.getTransactionsEC = function(errMsg) {
    var accNavObj = applicationManager.getNavigationManager();
    accNavObj.setCustomInfo("frmTransactions", errMsg);
    alert("Data Not Available #######" + JSON.stringify(errMsg));
    accNavObj.navigateTo("frmTransactions");
  };

  return Transaction_PresentationController;
});