define([], function () { 

  /**
     * User defined business controller
     * @constructor
     * @extends kony.mvc.Business.Controller
     */
  function TransferManager() { 

    kony.mvc.Business.Controller.call(this); 

  } 

  inheritsFrom(TransferManager, kony.mvc.Business.Delegator); 

  /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller gets initialized
     * @method
     */
  TransferManager.prototype.initializeBusinessController = function() { 

  }; 

  /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller is told to execute a command
     * @method
     * @param {Object} kony.mvc.Business.Command Object
     */
  TransferManager.prototype.execute = function(command) { 

    kony.mvc.Business.Controller.prototype.execute.call(this, command);

  };
  
  TransferManager.prototype.addTransactionToDB = function(params, presentationSuccessCallback, presentationErrorCallback) {
    var self = this;
    var transactionRepoObj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("transaction");
    transactionRepoObj.customVerb("doTransfer", params, getTransferCompletionCallback);
    function getTransferCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      kony.print("Data from getTransferCompletionCallback ..." + JSON.stringify(obj));
      if(obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      } else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  };

  return TransferManager;

});