define([], function() {
  /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
  function Loan_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(Loan_PresentationController, kony.mvc.Presentation.BasePresenter);

  /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
  Loan_PresentationController.prototype.initializePresentationController = function() {

  };
  Loan_PresentationController.prototype.navigateToLoanForm = function() {
    var interestRate = "4.3";
    var data = {
      "interestRate": {
        "interestRateValue": interestRate
      }
    };
    var navManager = applicationManager.getNavigationManager();
    navManager.setCustomInfo("frmLoan", data);
    navManager.navigateTo("frmLoan");
  };

  Loan_PresentationController.prototype.applyLoan = function(amount, terms, interest, purpose, idnumber, phone, email){
    var params = {
      "loanid": Math.random().toString(36).substring(2,7).toUpperCase(),
      "amount": amount,
      "terms":terms,
      "interest": interest,
      "purpose": purpose,
      "idnumber": idnumber,
      "phone": phone,
      "email": email
    };

    var loanManager = applicationManager.getLoanManager();
    loanManager.applyLoan(params, this.applyLoanSC.bind(this), this.applyLoanEC.bind(this));
  };

  Loan_PresentationController.prototype.applyLoanSC = function(response){
    var message = null;
    if(response[0].loanid){
      message = "Your loan application accepted. Reference: " + response[0].loanid;
    } else {
      message = "There is a problem  in accepting your loan application. Please contact support";
    }
    var data = {
      "loanReference": {
        "message": message
      }
    };
    var navManager = applicationManager.getNavigationManager();
    alert("Loan Created Sucessfully #### . "+JSON.stringify(response));
    navManager.setCustomInfo("frmLoan", data);
    navManager.navigateTo("frmLoan");
  };

  Loan_PresentationController.prototype.applyLoanEC = function(errMsg){
    var navManager = applicationManager.getNavigationManager();
    navManager.setCustomInfo("frmLoan", errMsg);
    alert("Error creating loan Record ###### "+JSON.stringify(errMsg));
    navManager.navigateTo("frmLoan");
  };


  return Loan_PresentationController;
});