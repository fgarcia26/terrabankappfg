define([], function () { 

  /**
     * User defined business controller
     * @constructor
     * @extends kony.mvc.Business.Controller
     */
  function TransactionManager() { 

    kony.mvc.Business.Controller.call(this); 

  } 

  inheritsFrom(TransactionManager, kony.mvc.Business.Delegator); 

  /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller gets initialized
     * @method
     */
  TransactionManager.prototype.initializeBusinessController = function() { 

  }; 

  /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller is told to execute a command
     * @method
     * @param {Object} kony.mvc.Business.Command Object
     */
  TransactionManager.prototype.execute = function(command) { 

    kony.mvc.Business.Controller.prototype.execute.call(this, command);

  };
  TransactionManager.prototype.getTransactions = function(presentationSuccessCallback, presentationErrorCallback) {
    var transactionList = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("transaction");
    transactionList.customVerb("getTransaction", {}, getTransactionCompletionCallback);
    function getTransactionCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      kony.print("Data from getAccounts ..." + JSON.stringify(obj));
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      } else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  };
  return TransactionManager;

});