define({

  getAccountsSC: function(response) {
    alert("In PC Extn");
    kony.print("Actual Acc Data" + JSON.stringify(response));

    try {
      var length = response.length;
      for(var i=0;i<length;i++) {
        var accountidBeforeMask = response[i]["accountid"];
        var l = accountidBeforeMask.length;
        var firstPart = accountidBeforeMask.slice(0, l-2);
        var secondPart = accountidBeforeMask.slice(l-2, l);
        firstPart = firstPart.replace(/./g, "X");
        response[i]["accountidmasked"] = firstPart + secondPart;
        kony.print("Modified Acc Data "+JSON.stringify(response));
      }
    } catch(exp) {
      kony.print("error " + JSON.stringify(exp));
    }
    var accNavObj = applicationManager.getNavigationManager();
    alert("Response at Account_PC . "+JSON.stringify(response));
    accNavObj.setCustomInfo("frmHome", response);
    accNavObj.navigateTo("frmHome");
  },

  navigateToUpdateAccForm: function() {
    var navigationObj = applicationManager.getNavigationManager();
    var response = navigationObj.getCustomInfo("frmHome");
    var resList = [];

    try {
      var data = response;
      var length = data.length;
      var resObj = [];

      for(var i = 0; i < length; i++) {
        resObj = [data[i]["accountid"], data[i]["accountname"]];
        resList.push(resObj);
      }
    } catch (exp) {
      kony.print("error");
    }
    var listData = {"masterData": resList};
    navigationObj.setCustomInfo("frmChangeName", listData);
    navigationObj.navigateTo("frmChangeName");
  },
  
  updateAccountName: function(){
    var params = {
      accountid: accountId,
      accountname: accountName
    };
    var accountManager = application.getAccountManager();
    accountManager.updateAccount(params, this.updateAccountSC.bind(this), this.updateAccountEC.bind(this));
  },

  updateAccountSC: function(response) {
    alert("Account Name Updated Successfully");
    var accountManager = applicationManager.getAccountManager();
    accountManager.getAccounts(this.getAccountsSC.bind(this), this.getAccountsEC.bind(this));
  },

  updateAccountEC: function(errMsg) {
    var accNavObj = applicationManager.getNavigationManager();
    accNavObj.setCustomInfo("frmHome", errMsg);
    alert("Unable to Update Account ##### "+JSON.stringify(errMsg));
    accNavObj.navigateTo("frmHome");
  }

});