define([], function() {
  /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
  function Account_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(Account_PresentationController, kony.mvc.Presentation.BasePresenter);

  /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
  Account_PresentationController.prototype.initializePresentationController = function() {

  };

  Account_PresentationController.prototype.getAccountsandShowHome = function() {
    var accountManger = applicationManager.getAccountManager();
    accountManger.getAccounts(this.getAccountsSC.bind(this), this.getAccountsEC.bind(this));
  };

  Account_PresentationController.prototype.getAccountsSC = function(response){
    var accNavObj = applicationManager.getNavigationManager();
    //alert("Response at Account_PC. "+JSON.stringify(response);
    accNavObj.setCustomInfo("frmHome", response);
    accNavObj.navigateTo("frmHome"); 
  };

  /* getAccounts Error Callback */
  Account_PresentationController.prototype.getAccountsEC = function(errMsg){
    var accNavobj = applicationManager.getNavigationManager();
    //alert("Error at Account_PC. "+JSON.stringify(response));
    accNavobj.setCustomInfo("frm Home", errMsg);
    alert("Data Not Available ###### "+JSON.stringify(errMsg));
    accNavobj.navigateTo("frmHome");
  };

  Account_PresentationController.prototype.navigateToHomeUsingNavObj = function() {
    var accNavObj = applicationManager.getNavigationManager();

    accNavObj.goBack("frmTransactions");
  };

  return Account_PresentationController;
});