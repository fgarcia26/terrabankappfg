define({
  saveTransactionToDB: function(fromaccountid, toaccountid, amount, currency) {
    if (currency == "INR") {
      amount = amount/70;
    }
    var params = {
      "tid": Math.random().toString(36).substring(7),
      "fromaccountid": fromaccountid,
      "toaccountid": toaccountid,
      "amount": amount,
      "notes": "From MVC 2.0 client Extention"
    };

    var transferManager = applicationManager.getTransferManager();
    transferManager.addTransactionToDB(params, this.addTransactionSC.bind(this), this.addTransactionEC.bind(this));
  }
});