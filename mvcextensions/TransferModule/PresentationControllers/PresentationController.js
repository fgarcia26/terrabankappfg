define([], function() {
  /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
  function Transfer_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(Transfer_PresentationController, kony.mvc.Presentation.BasePresenter);

  /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
  Transfer_PresentationController.prototype.initializePresentationController = function() {

  };

  Transfer_PresentationController.prototype.getAccountsandLoadTransferForm = function() {
    var navigationObj = applicationManager.getNavigationManager();
    var response = navigationObj.getCustomInfo("frmHome");
    var resList = [];

    try {
      var data = response;
      var length = data.length;
      var resObj = [];
      for(var i = 0; i < length; i++) {
        resObj = [
          data[i]["accountid"],
          data[i]["accountname"] ];
        resList.push(resObj);
      }
      var listData = { "masterData": resList };
      navigationObj.setCustomInfo("frmTransfer", listData);
      navigationObj.navigateTo("frmTransfer");
    }catch(exp) {
      kony.print("error");

    }
  };

  Transfer_PresentationController.prototype.saveTransactionToDB = function(fromaccountid, toaccountid, amount) {
    var params = {
      "tid": Math.random().toString(36).substring(7),
      "fromaccountid": fromaccountid,
      "toaccountid": toaccountid,
      "amount": amount,
      "notes": "From MVC2.0 client"
    };
    var transferManager = applicationManager.getTransferManager();
    transferManager.addTransactionToDB(params, this.addTransactionSC.bind(this), this.addTransactionEC.bind(this));
  };

  Transfer_PresentationController.prototype.addTransactionSC = function(response) {
    //var navObj = applicationManager.getNavigationManager();
    alert("Transaction Added Successfully #### . " + JSON.stringify(response));
    //navObj.navigateTo("frmTransfer");
    var transactionModule = applicationManager.getModule("TransactionModule");
    transactionModule.presentationController.getTransactionsAndShow();
  };

  Transfer_PresentationController.prototype.addTransactionEC = function(errMsg) {
    var navObj = applicationManager.getNavigationManager();
    navObj.setCustomInfo("frmTransfer", errMsg);
    alert("Error Added Transaction Record #### ." + JSON.stringify(errMsg));
    navObj.navigateTo("frmTransfer");
  };

  return Transfer_PresentationController;
});