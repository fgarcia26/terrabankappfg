define([], function () { 

  /**
     * User defined business controller
     * @constructor
     * @extends kony.mvc.Business.Controller
     */
  function AccountManager() { 

    kony.mvc.Business.Controller.call(this); 

  } 

  inheritsFrom(AccountManager, kony.mvc.Business.Delegator); 

  /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller gets initialized
     * @method
     */
  AccountManager.prototype.initializeBusinessController = function() { 

  }; 

  /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller is told to execute a command
     * @method
     * @param {Object} kony.mvc.Business.Command Object
     */
  AccountManager.prototype.execute = function(command) { 

    kony.mvc.Business.Controller.prototype.execute.call(this, command);

  };

  AccountManager.prototype.getAccounts = function (presentationSuccessCallback, presentationErrorCallback) {
    var self = this;
    var accountList= kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("account");
    accountList.customVerb('getAccounts', {}, getAccountsCompletionCallback);
    function getAccountsCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse( status, data, error);
      kony.print("Data from getAccounts ... "+JSON.stringify(obj));
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      } else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  };

  return AccountManager;

});