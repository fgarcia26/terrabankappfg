define({

  updateAccount: function(params, presentationSuccessCallback, presentationErrorCallback){
    alert("in Account Manager extn for update account");
    var self = this;
    var accountList = kont.mvc.MDAAplication.getSharedInstance().getRepoManager().getRepository("account");
    accountList.customVerb("updateAccount", params, updateAccountCompletionCallback);
    function updateAccountCompletionCallback(status, data, error){
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      kony.print("data from update account ..."+JSON.stringify(obj));
      if(obj["status"] === true){
        presentationSuccessCallback(obj["data"]);
      }else{
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  }

});