define([], function () { 

  /**
     * User defined business controller
     * @constructor
     * @extends kony.mvc.Business.Controller
     */
  function LoanManager() { 

    kony.mvc.Business.Controller.call(this); 

  } 

  inheritsFrom(LoanManager, kony.mvc.Business.Delegator); 

  /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller gets initialized
     * @method
     */
  LoanManager.prototype.initializeBusinessController = function() { 

  }; 

  /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller is told to execute a command
     * @method
     * @param {Object} kony.mvc.Business.Command Object
     */
  LoanManager.prototype.execute = function(command) { 

    kony.mvc.Business.Controller.prototype.execute.call(this, command);

  };
  
  LoanManager.prototype.applyLoan = function(params, presentationSuccesCallback, presentationErrorCallback){
    var self = this;
    alert("Input params . "+JSON.stringify(params));
  	var loanRepoObj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("loan");
    loanRepoObj.customVerb("applyLoan", params, addLoanCompletionCallback);
    function addLoanCompletionCallback(status, data, error){
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      kony.print("Data from apploan ..."+JSON.stringify(obj));
      if (obj["status"] === true){
        presentationSuccesCallback(obj["data"]);
      } else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  };

  return LoanManager;

});