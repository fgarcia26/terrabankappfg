define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function accountRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	accountRepository.prototype = Object.create(BaseRepository.prototype);
	accountRepository.prototype.constructor = accountRepository;

	//For Operation 'getAccounts' with service id 'terrabankappfg_account_get5733'
	accountRepository.prototype.getAccounts = function(params,onCompletion){
		return accountRepository.prototype.customVerb('getAccounts',params, onCompletion);
	};
	//For Operation 'updateAccount' with service id 'terrabankappfg_account_update3412'
	accountRepository.prototype.updateAccount = function(params,onCompletion){
		return accountRepository.prototype.customVerb('updateAccount',params, onCompletion);
	};
	
	
	return accountRepository;
})