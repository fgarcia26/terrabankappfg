define([],function(){
	var mappings = {
		"accountid" : "accountid",
		"accountname" : "accountname",
		"balance" : "balance",
	};
	Object.freeze(mappings);
	
	var typings = {
		"accountid" : "string",
		"accountname" : "string",
		"balance" : "number",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"accountid",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "TerraBankServiceFG",
		tableName : "account"
	};
	Object.freeze(config);
	
	return config;
})
