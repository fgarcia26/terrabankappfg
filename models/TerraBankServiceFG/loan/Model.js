define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "loan", "objectService" : "TerraBankServiceFG"};
	
	var setterFunctions = {
		loanid : function(val, state){
			context["field"]  = "loanid";
			context["metadata"] = (objectMetadata ? objectMetadata["loanid"] : null);
			state['loanid'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		amount : function(val, state){
			context["field"]  = "amount";
			context["metadata"] = (objectMetadata ? objectMetadata["amount"] : null);
			state['amount'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		terms : function(val, state){
			context["field"]  = "terms";
			context["metadata"] = (objectMetadata ? objectMetadata["terms"] : null);
			state['terms'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		interest : function(val, state){
			context["field"]  = "interest";
			context["metadata"] = (objectMetadata ? objectMetadata["interest"] : null);
			state['interest'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		email : function(val, state){
			context["field"]  = "email";
			context["metadata"] = (objectMetadata ? objectMetadata["email"] : null);
			state['email'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		idnumber : function(val, state){
			context["field"]  = "idnumber";
			context["metadata"] = (objectMetadata ? objectMetadata["idnumber"] : null);
			state['idnumber'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		phone : function(val, state){
			context["field"]  = "phone";
			context["metadata"] = (objectMetadata ? objectMetadata["phone"] : null);
			state['phone'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		isapproved : function(val, state){
			context["field"]  = "isapproved";
			context["metadata"] = (objectMetadata ? objectMetadata["isapproved"] : null);
			state['isapproved'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		isclosed : function(val, state){
			context["field"]  = "isclosed";
			context["metadata"] = (objectMetadata ? objectMetadata["isclosed"] : null);
			state['isclosed'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		purpose : function(val, state){
			context["field"]  = "purpose";
			context["metadata"] = (objectMetadata ? objectMetadata["purpose"] : null);
			state['purpose'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		createdtimestamp : function(val, state){
			context["field"]  = "createdtimestamp";
			context["metadata"] = (objectMetadata ? objectMetadata["createdtimestamp"] : null);
			state['createdtimestamp'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
	};
	
	
	//Create the Model Class
	function loan(defaultValues){
		var privateState = {};
			context["field"]  = "loanid";
			context["metadata"] = (objectMetadata ? objectMetadata["loanid"] : null);
			privateState.loanid = defaultValues?(defaultValues["loanid"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["loanid"], context):null):null;
			context["field"]  = "amount";
			context["metadata"] = (objectMetadata ? objectMetadata["amount"] : null);
			privateState.amount = defaultValues?(defaultValues["amount"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["amount"], context):null):null;
			context["field"]  = "terms";
			context["metadata"] = (objectMetadata ? objectMetadata["terms"] : null);
			privateState.terms = defaultValues?(defaultValues["terms"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["terms"], context):null):null;
			context["field"]  = "interest";
			context["metadata"] = (objectMetadata ? objectMetadata["interest"] : null);
			privateState.interest = defaultValues?(defaultValues["interest"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["interest"], context):null):null;
			context["field"]  = "email";
			context["metadata"] = (objectMetadata ? objectMetadata["email"] : null);
			privateState.email = defaultValues?(defaultValues["email"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["email"], context):null):null;
			context["field"]  = "idnumber";
			context["metadata"] = (objectMetadata ? objectMetadata["idnumber"] : null);
			privateState.idnumber = defaultValues?(defaultValues["idnumber"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["idnumber"], context):null):null;
			context["field"]  = "phone";
			context["metadata"] = (objectMetadata ? objectMetadata["phone"] : null);
			privateState.phone = defaultValues?(defaultValues["phone"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["phone"], context):null):null;
			context["field"]  = "isapproved";
			context["metadata"] = (objectMetadata ? objectMetadata["isapproved"] : null);
			privateState.isapproved = defaultValues?(defaultValues["isapproved"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isapproved"], context):null):null;
			context["field"]  = "isclosed";
			context["metadata"] = (objectMetadata ? objectMetadata["isclosed"] : null);
			privateState.isclosed = defaultValues?(defaultValues["isclosed"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isclosed"], context):null):null;
			context["field"]  = "purpose";
			context["metadata"] = (objectMetadata ? objectMetadata["purpose"] : null);
			privateState.purpose = defaultValues?(defaultValues["purpose"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["purpose"], context):null):null;
			context["field"]  = "createdtimestamp";
			context["metadata"] = (objectMetadata ? objectMetadata["createdtimestamp"] : null);
			privateState.createdtimestamp = defaultValues?(defaultValues["createdtimestamp"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["createdtimestamp"], context):null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"loanid" : {
					get : function(){
						context["field"]  = "loanid";
			        	context["metadata"] = (objectMetadata ? objectMetadata["loanid"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.loanid, context);},
					set : function(val){
						setterFunctions['loanid'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"amount" : {
					get : function(){
						context["field"]  = "amount";
			        	context["metadata"] = (objectMetadata ? objectMetadata["amount"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.amount, context);},
					set : function(val){
						setterFunctions['amount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"terms" : {
					get : function(){
						context["field"]  = "terms";
			        	context["metadata"] = (objectMetadata ? objectMetadata["terms"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.terms, context);},
					set : function(val){
						setterFunctions['terms'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"interest" : {
					get : function(){
						context["field"]  = "interest";
			        	context["metadata"] = (objectMetadata ? objectMetadata["interest"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.interest, context);},
					set : function(val){
						setterFunctions['interest'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"email" : {
					get : function(){
						context["field"]  = "email";
			        	context["metadata"] = (objectMetadata ? objectMetadata["email"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.email, context);},
					set : function(val){
						setterFunctions['email'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"idnumber" : {
					get : function(){
						context["field"]  = "idnumber";
			        	context["metadata"] = (objectMetadata ? objectMetadata["idnumber"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.idnumber, context);},
					set : function(val){
						setterFunctions['idnumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"phone" : {
					get : function(){
						context["field"]  = "phone";
			        	context["metadata"] = (objectMetadata ? objectMetadata["phone"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.phone, context);},
					set : function(val){
						setterFunctions['phone'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isapproved" : {
					get : function(){
						context["field"]  = "isapproved";
			        	context["metadata"] = (objectMetadata ? objectMetadata["isapproved"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isapproved, context);},
					set : function(val){
						setterFunctions['isapproved'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isclosed" : {
					get : function(){
						context["field"]  = "isclosed";
			        	context["metadata"] = (objectMetadata ? objectMetadata["isclosed"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isclosed, context);},
					set : function(val){
						setterFunctions['isclosed'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"purpose" : {
					get : function(){
						context["field"]  = "purpose";
			        	context["metadata"] = (objectMetadata ? objectMetadata["purpose"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.purpose, context);},
					set : function(val){
						setterFunctions['purpose'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"createdtimestamp" : {
					get : function(){
						context["field"]  = "createdtimestamp";
			        	context["metadata"] = (objectMetadata ? objectMetadata["createdtimestamp"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.createdtimestamp, context);},
					set : function(val){
						setterFunctions['createdtimestamp'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});
			
			//converts model object to json object.
			this.toJsonInternal = function() {
				return Object.assign({}, privateState);
			};

			//overwrites object state with provided json value in argument.
			this.fromJsonInternal = function(value) {
									privateState.loanid = value?(value["loanid"]?value["loanid"]:null):null;
					privateState.amount = value?(value["amount"]?value["amount"]:null):null;
					privateState.terms = value?(value["terms"]?value["terms"]:null):null;
					privateState.interest = value?(value["interest"]?value["interest"]:null):null;
					privateState.email = value?(value["email"]?value["email"]:null):null;
					privateState.idnumber = value?(value["idnumber"]?value["idnumber"]:null):null;
					privateState.phone = value?(value["phone"]?value["phone"]:null):null;
					privateState.isapproved = value?(value["isapproved"]?value["isapproved"]:null):null;
					privateState.isclosed = value?(value["isclosed"]?value["isclosed"]:null):null;
					privateState.purpose = value?(value["purpose"]?value["purpose"]:null):null;
					privateState.createdtimestamp = value?(value["createdtimestamp"]?value["createdtimestamp"]:null):null;
			};

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(loan);
	
	//Create new class level validator object
	BaseModel.Validator.call(loan);
	
	var registerValidatorBackup = loan.registerValidator;
	
	loan.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( loan.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'applyLoan' with service id 'terrabankappfg_loan_create5119'
	loan.applyLoan = function(params, onCompletion){
		return loan.customVerb('applyLoan', params, onCompletion);
	};
	//For Operation 'getLoanAccounts' with service id 'terrabankappfg_loan_get5844'
	loan.getLoanAccounts = function(params, onCompletion){
		return loan.customVerb('getLoanAccounts', params, onCompletion);
	};
	
	var relations = [
	];
	
	loan.relations = relations;
	
	loan.prototype.isValid = function(){
		return loan.isValid(this);
	};
	
	loan.prototype.objModelName = "loan";
	
	/*This API allows registration of preprocessors and postprocessors for model.
	 *It also fetches object metadata for object. 
	 *Options Supported
	 *preProcessor  - preprocessor function for use with setters.
	 *postProcessor - post processor callback for use with getters.
	 *getFromServer - value set to true will fetch metadata from network else from cache.
	 */
	loan.registerProcessors = function(options, successCallback, failureCallback) {
	
		if(!options) {
			options = {};
		}
			
		if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
			preProcessorCallback = options["preProcessor"];
		}
		
		if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])){
			postProcessorCallback = options["postProcessor"];
		}
		
		function metaDataSuccess(res) {
			objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
			successCallback();
		}
		
		function metaDataFailure(err) {
			failureCallback(err);
		}
		
		kony.mvc.util.ProcessorUtils.getMetadataForObject("TerraBankServiceFG", "loan", options, metaDataSuccess, metaDataFailure);
	};
	
	//clone the object provided in argument.
	loan.clone = function(objectToClone) {
		var clonedObj = new loan();
		clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
		return clonedObj;
	};
	
	return loan;
});