define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function loanRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	loanRepository.prototype = Object.create(BaseRepository.prototype);
	loanRepository.prototype.constructor = loanRepository;

	//For Operation 'applyLoan' with service id 'terrabankappfg_loan_create5119'
	loanRepository.prototype.applyLoan = function(params,onCompletion){
		return loanRepository.prototype.customVerb('applyLoan',params, onCompletion);
	};
	//For Operation 'getLoanAccounts' with service id 'terrabankappfg_loan_get5844'
	loanRepository.prototype.getLoanAccounts = function(params,onCompletion){
		return loanRepository.prototype.customVerb('getLoanAccounts',params, onCompletion);
	};
	
	
	return loanRepository;
})