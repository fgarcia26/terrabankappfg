define([],function(){
	var mappings = {
		"loanid" : "loanid",
		"amount" : "amount",
		"terms" : "terms",
		"interest" : "interest",
		"email" : "email",
		"idnumber" : "idnumber",
		"phone" : "phone",
		"isapproved" : "isapproved",
		"isclosed" : "isclosed",
		"purpose" : "purpose",
		"createdtimestamp" : "createdtimestamp",
	};
	Object.freeze(mappings);
	
	var typings = {
		"loanid" : "string",
		"amount" : "number",
		"terms" : "number",
		"interest" : "number",
		"email" : "string",
		"idnumber" : "string",
		"phone" : "number",
		"isapproved" : "number",
		"isclosed" : "number",
		"purpose" : "string",
		"createdtimestamp" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"loanid",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "TerraBankServiceFG",
		tableName : "loan"
	};
	Object.freeze(config);
	
	return config;
})
