define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function transactionRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	transactionRepository.prototype = Object.create(BaseRepository.prototype);
	transactionRepository.prototype.constructor = transactionRepository;

	//For Operation 'doTransfer' with service id 'terrabankappfg_transaction_create8560'
	transactionRepository.prototype.doTransfer = function(params,onCompletion){
		return transactionRepository.prototype.customVerb('doTransfer',params, onCompletion);
	};
	//For Operation 'getTransaction' with service id 'terrabankappfg_transaction_get5877'
	transactionRepository.prototype.getTransaction = function(params,onCompletion){
		return transactionRepository.prototype.customVerb('getTransaction',params, onCompletion);
	};
	
	
	return transactionRepository;
})