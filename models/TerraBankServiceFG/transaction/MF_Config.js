define([],function(){
	var mappings = {
		"tid" : "tid",
		"toaccountid" : "toaccountid",
		"fromaccountid" : "fromaccountid",
		"amount" : "amount",
		"notes" : "notes",
		"createdtimestamp" : "createdtimestamp",
	};
	Object.freeze(mappings);
	
	var typings = {
		"tid" : "string",
		"toaccountid" : "string",
		"fromaccountid" : "string",
		"amount" : "number",
		"notes" : "string",
		"createdtimestamp" : "date",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"tid",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "TerraBankServiceFG",
		tableName : "transaction"
	};
	Object.freeze(config);
	
	return config;
})
