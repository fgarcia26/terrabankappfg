define([],function(){
	var repoMapping = {
		account  : {
			model : "TerraBankServiceFG/account/Model",
			config : "TerraBankServiceFG/account/MF_Config",
			repository : "TerraBankServiceFG/account/Repository",
		},
		transaction  : {
			model : "TerraBankServiceFG/transaction/Model",
			config : "TerraBankServiceFG/transaction/MF_Config",
			repository : "TerraBankServiceFG/transaction/Repository",
		},
		loan  : {
			model : "TerraBankServiceFG/loan/Model",
			config : "TerraBankServiceFG/loan/MF_Config",
			repository : "TerraBankServiceFG/loan/Repository",
		},
	};
	
	return repoMapping;
})